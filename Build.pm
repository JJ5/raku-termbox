use v6;
use LibraryMake;

class Build {
    method build ($cwd) {
        my %vars = get-vars($cwd);
        %vars<termbox> = $*VM.platform-library-name('termbox'.IO);
        %vars<python>  = $*DISTRO ~~ /centos/ ?? 'python3' !! 'python';

        mkdir $_ unless .IO.e given 'resources/libraries';

        process-makefile($cwd, %vars);

        shell(%vars<MAKE>);
    }
}
