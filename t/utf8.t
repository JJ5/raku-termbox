#!/usr/bin/env raku

use Test;
use NativeCall;

subtest 'Imported' => {
    use Termbox :subs;

    my Str $string;
    my uint32 $uint;

    is try { tb-encode-string( '' ) }, Nil;
    is tb-encode-string( 'a'  ), 97;
    is tb-encode-string( 'á'  ), 225;
    is tb-encode-string( '字' ), 23383;

    is try { tb-decode-string( 0 ) }, Nil;
    is tb-decode-string( 97    ), 'a';
    is tb-decode-string( 225   ), 'á';
    is tb-decode-string( 23383 ), '字';
};

subtest 'Not imported' => {
    use Termbox;

    my uint32 $uint;

    is try { Termbox::encode-string( '' ) }, Nil;
    is Termbox::encode-string( 'a'  ), 97;
    is Termbox::encode-string( 'á'  ), 225;
    is Termbox::encode-string( '字' ), 23383;

    is try { Termbox::decode-string( 0 ) }, Nil;
    is Termbox::decode-string( 97    ), 'a';
    is Termbox::decode-string( 225   ), 'á';
    is Termbox::decode-string( 23383 ), '字';
};

done-testing;
