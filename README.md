NAME
====

Termbox

DESCRIPTION
===========

Termbox is "a library for writing text-based user interfaces". This module provides Raku bindings to this library.

SYNOPSIS
========

    use Termbox :ALL;

    if tb-init() -> $ret {
        note "tb-init() failed with error code $ret";
        exit 1;
    }

    END tb-shutdown;

    for "Press ESC to exit!".encode.list -> $c {
        state $x = 5;
        tb-change-cell( $x++, 5, $c, TB_COLOR_BLACK, TB_COLOR_WHITE );
    }

    tb-present;

    my $events = Supplier.new;
    start {
        while tb-poll-event( my $ev = Termbox::Event.new ) { $events.emit: $ev }
    }

    react whenever $events.Supply -> $ev {
        given $ev.type {
            when TB_EVENT_KEY {
                given $ev.key {
                    when TB_KEY_ESC { done }
                }
            }
        }
    }

FUNCTIONS
=========

None of the following functions are exported by default. They can be accessed through the `Termbox` package (as in `Termbox::init`) or imported with the `:subs` tag. When imported, they gain the `tb-` prefix (such that `init` becomes `tb-init`, etc).

Basic functions
---------------

### init

### shutdown

### width

### height

### clear

### present

### put-cell

### change-cell

### blit

### cell-buffer

### select-input-mode

### select-output-mode

### peek-event

### poll-event

### utf8-char-to-unicode

See also `encode-string` for a higher-level version of this function.

### utf8-unicode-to-char

See also `decode-string` for a higher-level version of this function.

Additional functions
--------------------

The following functions are added as a convenience for the UTF-8 helpers provided by Termbox itself. These are likely much simpler to use in Raku code.

### encode-string

Takes a non-empty string and returns an Int representing the number used to encode it by Termbox.

On error throws an exception.

### decode-string

Takes an Int with the integer representation of a string (such as that returned by `encode-string` and returns it decoded as a Str.

On error throws an exception.

CONSTANTS
=========

Like with the functions described above, none of the following constants are exported by default. They can be accessed through the `Termbox` package (as in `Termbox::WHITE`) or imported with the different tags described below.

When imported, they gain the `TB_` prefix (such that `WHITE` becomes `TB_WHITE`, etc).

Keys
----

Imported with the `:keys` tag.

These are a safe subset of terminfo keys, which exist on all popular terminals. Use only these to stay "truly portable", according to the Termbox documentation.

  * `KEY_F1`

  * `KEY_F2`

  * `KEY_F3`

  * `KEY_F4`

  * `KEY_F5`

  * `KEY_F6`

  * `KEY_F7`

  * `KEY_F8`

  * `KEY_F9`

  * `KEY_F10`

  * `KEY_F11`

  * `KEY_F12`

  * `KEY_INSERT`

  * `KEY_DELETE`

  * `KEY_HOME`

  * `KEY_END`

  * `KEY_PGUP`

  * `KEY_PGDN`

  * `KEY_ARROW_UP`

  * `KEY_ARROW_DOWN`

  * `KEY_ARROW_LEFT`

  * `KEY_ARROW_RIGHT`

  * `KEY_MOUSE_LEFT`

  * `KEY_MOUSE_RIGHT`

  * `KEY_MOUSE_MIDDLE`

  * `KEY_MOUSE_RELEASE`

  * `KEY_MOUSE_WHEEL_UP`

  * `KEY_MOUSE_WHEEL_DOWN`

The rest are ASCII code points below `SPACE` character and a `BACKSPACE` key.

  * `KEY_CTRL_TILDE`

  * `KEY_CTRL_2`

  * `KEY_CTRL_A`

  * `KEY_CTRL_B`

  * `KEY_CTRL_C`

  * `KEY_CTRL_D`

  * `KEY_CTRL_E`

  * `KEY_CTRL_F`

  * `KEY_CTRL_G`

  * `KEY_BACKSPACE`

  * `KEY_CTRL_H`

  * `KEY_TAB`

  * `KEY_CTRL_I`

  * `KEY_CTRL_J`

  * `KEY_CTRL_K`

  * `KEY_CTRL_L`

  * `KEY_ENTER`

  * `KEY_CTRL_M`

  * `KEY_CTRL_N`

  * `KEY_CTRL_O`

  * `KEY_CTRL_P`

  * `KEY_CTRL_Q`

  * `KEY_CTRL_R`

  * `KEY_CTRL_S`

  * `KEY_CTRL_T`

  * `KEY_CTRL_U`

  * `KEY_CTRL_V`

  * `KEY_CTRL_W`

  * `KEY_CTRL_X`

  * `KEY_CTRL_Y`

  * `KEY_CTRL_Z`

  * `KEY_ESC`

  * `KEY_CTRL_LSQ_BRACKET`

  * `KEY_CTRL_3`

  * `KEY_CTRL_4`

  * `KEY_CTRL_BACKSLASH`

  * `KEY_CTRL_5`

  * `KEY_CTRL_RSQ_BRACKET`

  * `KEY_CTRL_6`

  * `KEY_CTRL_7`

  * `KEY_CTRL_SLASH`

  * `KEY_CTRL_UNDERSCORE`

  * `KEY_SPACE`

  * `KEY_BACKSPACE2`

  * `KEY_CTRL_8`

These are modifier constants. `MOD_MOTION` in a mouse event indicates a dragging event.

  * `MOD_ALT`

  * `MOD_MOTION`

Styles
------

Imported with the `:styles` tag.

  * `DEFAULT`

  * `BLACK`

  * `RED`

  * `GREEN`

  * `YELLOW`

  * `BLUE`

  * `MAGENTA`

  * `CYAN`

  * `WHITE`

These are font attributes. It is possible to use multiple attributes by combining them using bitwise OR (`+|`). Although, colors cannot be combined, you can combine attributes and a single color.

  * `BOLD`

  * `UNDERLINE`

  * `REVERSE`

Errors
------

Imported with the `:errors` tag.

  * `EUNSUPPORTED_TERMINAL`

  * `EFAILED_TO_OPEN_TTY`

  * `EPIPE_TRAP_ERROR`

Events
------

Imported with the `:events` tag.

  * `EVENT_KEY`

  * `EVENT_RESIZE`

  * `EVENT_MOUSE`

Modes
-----

Imported with the `:modes` tag.

  * `HIDE_CURSOR`

  * `INPUT_CURRENT`

  * `INPUT_ESC`

  * `INPUT_ALT`

  * `INPUT_MOUSE`

  * `OUTPUT_CURRENT`

  * `OUTPUT_NORMAL`

  * `OUTPUT_256`

  * `OUTPUT_216`

  * `OUTPUT_GRAYSCALE`

AUTHOR
======

José Joaquín Atria <jjatria@cpan.org>

COPYRIGHT AND LICENSE
=====================

Copyright 2020 José Joaquín Atria

This library is free software; you can redistribute it and/or modify it under the Artistic License 2.0.

